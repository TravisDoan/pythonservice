class DailyStocksInfo:
    def __init__(self, symbol, symbolID, exchange, stockType, confirmNo, side, totalVol, totalPrice, tradingDate,
                 tradingTime, createdDate):
        self.symbol = symbol,
        self.symbolID = symbolID,
        self.exchange = exchange,
        self.stockType = stockType,
        self.confirmNo = confirmNo,
        self.side = side,
        self.totalVol = totalVol,
        self.totalPrice = totalPrice,
        self.tradingDate = tradingDate,
        self.tradingTime = tradingTime,
        self.createdDate = createdDate,
