class Trading(object):
    def __init__(self, stockNo, stockSymbol, stockType, ceiling, floor, bigLotValue, securityName, sectorNo, designated,
                 suspension, delist, haltResumeFlag, split, benefit, meeting, notice, clientIDRequest, couponRate,
                 issueDate, matureDate, avrPrice, parValue, sDCFlag, priorClosePrice, priorCloseDate, projectOpen,
                 openPrice, last, lastVol, lastVal, highest, lowest, totalShare, totalValue, accumulateDeal, bigDeal,
                 bigVol, bigVal, oddDeal, oddVol, oddVal, best1Bid, best1BidVolume, best2Bid, best2BidVolume, best3Bid,
                 best3BidVolume, best1Offer, best1OfferVolume, best2Offer, best2OfferVolume, best3Offer,
                 best3OfferVolume, dateReport, currentRoom, totalRoom, match1, matchVolume1, match2, matchVolume2,
                 other_room, room, sellPrice3, sellPrice2, sellPrice1, buyPrice3, buyPrice2, buyPrice1, sellQtty3,
                 sellQtty2, sellQtty1, buyQtty3, buyQtty2, buyQtty1, changePrice, closePrice_Adjusted, rate_Adjusted,
                 highest_Adjusted, lowest_Adjusted, kLGD_ThoaThuan, gTGD_ThoaThuan, openPrice_Adjusted):
        self.stockNo = stockNo
        self.stockSymbol = stockSymbol
        self.stockType = stockType
        self.ceiling = ceiling
        self.floor = floor
        self.bigLotValue = bigLotValue
        self.securityName = securityName
        self.sectorNo = sectorNo
        self.designated = designated
        self.suspension = suspension
        self.delist = delist
        self.haltResumeFlag = haltResumeFlag
        self.split = split
        self.benefit = benefit
        self.meeting = meeting
        self.notice = notice
        self.clientIDRequest = clientIDRequest
        self.couponRate = couponRate
        self.issueDate = issueDate
        self.matureDate = matureDate
        self.avrPrice = avrPrice
        self.parValue = parValue
        self.sDCFlag = sDCFlag
        self.priorClosePrice = priorClosePrice
        self.priorCloseDate = priorCloseDate
        self.projectOpen = projectOpen
        self.openPrice = openPrice
        self.last = last
        self.lastVol = lastVol
        self.lastVal = lastVal
        self.highest = highest
        self.lowest = lowest
        self.totalShare = totalShare
        self.totalValue = totalValue
        self.accumulateDeal = accumulateDeal
        self.bigDeal = bigDeal
        self.bigVol = bigVol
        self.bigVal = bigVal
        self.oddDeal = oddDeal
        self.oddVol = oddVol
        self.oddVal = oddVal
        self.best1Bid = best1Bid
        self.best1BidVolume = best1BidVolume
        self.best2Bid = best2Bid
        self.best2BidVolume = best2BidVolume
        self.best3Bid = best3Bid
        self.best3BidVolume = best3BidVolume
        self.best1Offer = best1Offer
        self.best1OfferVolume = best1OfferVolume
        self.best2Offer = best2Offer
        self.best2OfferVolume = best2OfferVolume
        self.best3Offer = best3Offer
        self.best3OfferVolume = best3OfferVolume
        self.dateReport = dateReport
        self.currentRoom = currentRoom
        self.totalRoom = totalRoom
        self.match1 = match1
        self.matchVolume1 = matchVolume1
        self.match2 = match2
        self.matchVolume2 = matchVolume2
        self.other_room = other_room
        self.room = room
        self.sellPrice3 = sellPrice3
        self.sellPrice2 = sellPrice2
        self.sellPrice1 = sellPrice1
        self.buyPrice3 = buyPrice3
        self.buyPrice2 = buyPrice2
        self.buyPrice1 = buyPrice1
        self.sellQtty3 = sellQtty3
        self.sellQtty2 = sellQtty2
        self.sellQtty1 = sellQtty1
        self.buyQtty3 = buyQtty3
        self.buyQtty2 = buyQtty2
        self.buyQtty1 = buyQtty1
        self.changePrice = changePrice
        self.closePrice_Adjusted = closePrice_Adjusted
        self.rate_Adjusted = rate_Adjusted
        self.highest_Adjusted = highest_Adjusted
        self.lowest_Adjusted = lowest_Adjusted
        self.kLGD_ThoaThuan = kLGD_ThoaThuan
        self.gTGD_ThoaThuan = gTGD_ThoaThuan
        self.openPrice_Adjusted = openPrice_Adjusted


class TotalTrading(object):
    def __init__(self, VNIndex, totalTrade, totalShares, totalValues, upVolume, downVolume, noChangeVolume, advances,
                 declines, noChange, VN50Index, marketID, filter, time, dateReport, preVNIndex, indexChange, liveID,
                 lowest, highest, openIndex):
        self.VNIndex = VNIndex
        self.totalTrade = totalTrade
        self.totalShares = totalShares
        self.totalValues = totalValues
        self.upVolume = upVolume
        self.downVolume = downVolume
        self.noChangeVolume = noChangeVolume
        self.advances = advances
        self.declines = declines
        self.noChange = noChange
        self.VN50Index = VN50Index
        self.marketID = marketID
        self.filter = filter
        self.time = time
        self.dateReport = dateReport
        self.preVNIndex = preVNIndex
        self.indexChange = indexChange
        self.liveID = liveID
        self.lowest = lowest
        self.highest = highest
        self.openIndex = openIndex


class DailyMarketData(object):
    def __init__(self, ticker, exchangeID, openPrice, highest, lowest, avgPrice, closePrice, tradingDate, changePrice,
                 matchBuyTradeValue, matchBuyTradeQuantity, matchSellTradeValue, matchSellTradeQuantity, buySell,
                 totalTrade, totalValue):
        self.ticker = ticker
        self.exchangeID = exchangeID
        self.openPrice = openPrice
        self.highest = highest
        self.lowest = lowest
        self.avgPrice = avgPrice
        self.closePrice = closePrice
        self.tradingDate = tradingDate
        self.changePrice = changePrice
        self.matchBuyTradeValue = matchBuyTradeValue
        self.matchBuyTradeQuantity = matchBuyTradeQuantity
        self.matchSellTradeValue = matchSellTradeValue
        self.matchSellTradeQuantity = matchSellTradeQuantity
        self.buySell = buySell
        self.totalTrade = totalTrade
        self.totalValue = totalValue


class ForeignerTrading(object):
    def __init__(self, foreignTradingResultID, stockCode, remainForeignQtty, buyForeignQtty, buyForeignValue,
                 sellForeignQtty, sellForeignValue, tradingDate, buyForeignQttyKL, buyForeignValueKL, sellForeignQttyKL,
                 sellForeignValueKL, buyForeignQttyTT, buyForeignValueTT, sellForeignQttyTT, sellForeignValueTT):
        self.foreignTradingResultID = foreignTradingResultID
        self.stockCode = stockCode
        self.remainForeignQtty = remainForeignQtty
        self.buyForeignQtty = buyForeignQtty
        self.buyForeignValue = buyForeignValue
        self.sellForeignQtty = sellForeignQtty
        self.sellForeignValue = sellForeignValue
        self.tradingDate = tradingDate
        self.buyForeignQttyKL = buyForeignQttyKL
        self.buyForeignValueKL = buyForeignValueKL
        self.sellForeignQttyKL = sellForeignQttyKL
        self.sellForeignValueKL = sellForeignValueKL
        self.buyForeignQttyTT = buyForeignQttyTT
        self.buyForeignValueTT = buyForeignValueTT
        self.sellForeignQttyTT = sellForeignQttyTT
        self.sellForeignValueTT = sellForeignValueTT
