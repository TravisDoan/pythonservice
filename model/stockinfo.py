from protobuf3.message import Message
from protobuf3.fields import BytesField, StringField, DoubleField, MessageField


class FixSecurityDto(Message):
    pass


class List_FixSecurityDto(Message):
    pass


class FixIndexDto(Message):
    pass


class List_FixIndexDto(Message):
    pass


class FixBoardInfoDto(Message):
    pass


class List_FixBoardInfoDto(Message):
    pass


class List_MarketDataDto(Message):
    pass


class MarketDataDto(Message):
    pass


class AmqDto(Message):
    pass


class List_AmqDto(Message):
    pass

FixSecurityDto.add_field('TradingDate', StringField(field_number=1, optional=True))
FixSecurityDto.add_field('TradingTime', StringField(field_number=2, optional=True))
FixSecurityDto.add_field('Exchange', StringField(field_number=3, optional=True))
FixSecurityDto.add_field('StockNo', StringField(field_number=4, optional=True))
FixSecurityDto.add_field('StockSymbol', StringField(field_number=5, optional=True))
FixSecurityDto.add_field('StockType', StringField(field_number=6, optional=True))
FixSecurityDto.add_field('SecurityName', StringField(field_number=7, optional=True))
FixSecurityDto.add_field('IssueDate', StringField(field_number=8, optional=True))
FixSecurityDto.add_field('TradingUnit', StringField(field_number=9, optional=True))
FixSecurityDto.add_field('ParValue', StringField(field_number=10, optional=True))
FixSecurityDto.add_field('UnderlyingSymbol', StringField(field_number=11, optional=True))
FixSecurityDto.add_field('IssuerName', StringField(field_number=12, optional=True))
FixSecurityDto.add_field('CoverredWarrantType', StringField(field_number=13, optional=True))
FixSecurityDto.add_field('MaturityDate', StringField(field_number=14, optional=True))
FixSecurityDto.add_field('FirstTradingDate', StringField(field_number=15, optional=True))
FixSecurityDto.add_field('LastTradingDate', StringField(field_number=16, optional=True))
FixSecurityDto.add_field('ExercisePrice', StringField(field_number=17, optional=True))
FixSecurityDto.add_field('ExerciseRatio', StringField(field_number=18, optional=True))
FixSecurityDto.add_field('ListedShare', DoubleField(field_number=19, optional=True, default=0))
FixSecurityDto.add_field('OpenInterest', StringField(field_number=20, optional=True))
FixSecurityDto.add_field('OpenInterestChange', StringField(field_number=21, optional=True))
FixSecurityDto.add_field('SecSessionCode', StringField(field_number=22, optional=True))
FixSecurityDto.add_field('TradingStatus', StringField(field_number=23, optional=True))
FixSecurityDto.add_field('CaStatus', StringField(field_number=24, optional=True))
FixSecurityDto.add_field('RefPrice', StringField(field_number=25, optional=True))
FixSecurityDto.add_field('Ceiling', StringField(field_number=26, optional=True))
FixSecurityDto.add_field('Floor', StringField(field_number=27, optional=True))
FixSecurityDto.add_field('FloorPricePt', StringField(field_number=28, optional=True))
FixSecurityDto.add_field('CeilingPricePt', StringField(field_number=29, optional=True))
FixSecurityDto.add_field('MatchedPrice', StringField(field_number=30, optional=True))
FixSecurityDto.add_field('MatchedVolume', StringField(field_number=31, optional=True))
FixSecurityDto.add_field('PriceChange', StringField(field_number=32, optional=True))
FixSecurityDto.add_field('PriceChangePercentage', StringField(field_number=33, optional=True))
FixSecurityDto.add_field('PriorClosePrice', StringField(field_number=34, optional=True))
FixSecurityDto.add_field('PriorCloseDate', StringField(field_number=35, optional=True))
FixSecurityDto.add_field('Highest', StringField(field_number=36, optional=True))
FixSecurityDto.add_field('AvgPrice', StringField(field_number=37, optional=True))
FixSecurityDto.add_field('Lowest', StringField(field_number=38, optional=True))
FixSecurityDto.add_field('OfferCount', StringField(field_number=39, optional=True))
FixSecurityDto.add_field('TotalBidQty', DoubleField(field_number=40, optional=True, default=0))
FixSecurityDto.add_field('TotalOfferQty', DoubleField(field_number=41, optional=True, default=0))
FixSecurityDto.add_field('BidCount', StringField(field_number=42, optional=True))
FixSecurityDto.add_field('NmTotalTradedQty', DoubleField(field_number=43, optional=True, default=0))
FixSecurityDto.add_field('NmTotalTradedValue', DoubleField(field_number=44, optional=True, default=0))
FixSecurityDto.add_field('PtMatchQty', DoubleField(field_number=45, optional=True, default=0))
FixSecurityDto.add_field('PtMatchPrice', DoubleField(field_number=46, optional=True, default=0))
FixSecurityDto.add_field('PtTotalTradedQty', DoubleField(field_number=47, optional=True, default=0))
FixSecurityDto.add_field('PtTotalTradedValue', DoubleField(field_number=48, optional=True, default=0))
FixSecurityDto.add_field('TotalBuyTradingQtty', DoubleField(field_number=49, optional=True, default=0))
FixSecurityDto.add_field('BuyCount', StringField(field_number=50, optional=True))
FixSecurityDto.add_field('TotalBuyTradingValue', DoubleField(field_number=51, optional=True, default=0))
FixSecurityDto.add_field('TotalSellTradingQtty', DoubleField(field_number=52, optional=True, default=0))
FixSecurityDto.add_field('SellCount', StringField(field_number=53, optional=True))
FixSecurityDto.add_field('TotalSellTradingValue', DoubleField(field_number=54, optional=True, default=0))
FixSecurityDto.add_field('BuyForeignQtty', DoubleField(field_number=55, optional=True, default=0))
FixSecurityDto.add_field('BuyForeignValue', DoubleField(field_number=56, optional=True, default=0))
FixSecurityDto.add_field('SellForeignQtty', DoubleField(field_number=57, optional=True, default=0))
FixSecurityDto.add_field('SellForeignValue', DoubleField(field_number=58, optional=True, default=0))
FixSecurityDto.add_field('RemainForeignQtty', StringField(field_number=59, optional=True))
FixSecurityDto.add_field('TotalBidQttyOd', DoubleField(field_number=60, optional=True, default=0))
FixSecurityDto.add_field('TotalOfferQttyOd', DoubleField(field_number=61, optional=True, default=0))
FixSecurityDto.add_field('SecSessionStatus', StringField(field_number=62, optional=True))
FixSecurityDto.add_field('PutPrice', StringField(field_number=63, optional=True))
FixSecurityDto.add_field('PutVolumn', StringField(field_number=64, optional=True))
FixSecurityDto.add_field('MsgType', StringField(field_number=65, optional=True))
FixSecurityDto.add_field('SecSession', StringField(field_number=66, optional=True))
List_FixSecurityDto.add_field('items', MessageField(field_number=1, repeated=True, message_cls=FixSecurityDto))
FixIndexDto.add_field('IndexId', StringField(field_number=1, optional=True))
FixIndexDto.add_field('IndexValue', DoubleField(field_number=2, optional=True, default=0))
FixIndexDto.add_field('TradingDate', StringField(field_number=3, optional=True))
FixIndexDto.add_field('Time', StringField(field_number=4, optional=True))
FixIndexDto.add_field('Change', DoubleField(field_number=5, optional=True, default=0))
FixIndexDto.add_field('RatioChange', DoubleField(field_number=6, optional=True, default=0))
FixIndexDto.add_field('TotalTrade', StringField(field_number=7, optional=True))
FixIndexDto.add_field('TotalQtty', DoubleField(field_number=8, optional=True, default=0))
FixIndexDto.add_field('TotalValue', DoubleField(field_number=9, optional=True, default=0))
FixIndexDto.add_field('TypeIndex', StringField(field_number=10, optional=True))
FixIndexDto.add_field('IndexName', StringField(field_number=11, optional=True))
FixIndexDto.add_field('Advances', StringField(field_number=12, optional=True))
FixIndexDto.add_field('Nochanges', StringField(field_number=13, optional=True))
FixIndexDto.add_field('Declines', StringField(field_number=14, optional=True))
FixIndexDto.add_field('Ceiling', StringField(field_number=15, optional=True))
FixIndexDto.add_field('Floor', StringField(field_number=16, optional=True))
FixIndexDto.add_field('IndexTotalStock', StringField(field_number=17, optional=True))
FixIndexDto.add_field('PriorIndexVal', DoubleField(field_number=18, optional=True, default=0))
FixIndexDto.add_field('HighestIndex', DoubleField(field_number=19, optional=True, default=0))
FixIndexDto.add_field('LowestIndex', DoubleField(field_number=20, optional=True, default=0))
FixIndexDto.add_field('CloseIndex', DoubleField(field_number=21, optional=True, default=0))
FixIndexDto.add_field('MsgType', StringField(field_number=22, optional=True))
List_FixIndexDto.add_field('items', MessageField(field_number=1, repeated=True, message_cls=FixIndexDto))
FixBoardInfoDto.add_field('Exchange', StringField(field_number=1, optional=True))
FixBoardInfoDto.add_field('Session', StringField(field_number=2, optional=True))
FixBoardInfoDto.add_field('TradingSession', StringField(field_number=3, optional=True))
FixBoardInfoDto.add_field('TradeStatus', StringField(field_number=4, optional=True))
FixBoardInfoDto.add_field('Time', StringField(field_number=5, optional=True))
FixBoardInfoDto.add_field('SecSessionCode', StringField(field_number=6, optional=True))
FixBoardInfoDto.add_field('SecSessionStatus', StringField(field_number=7, optional=True))
FixBoardInfoDto.add_field('MsgType', StringField(field_number=8, optional=True))
FixBoardInfoDto.add_field('BoardStatus', StringField(field_number=9, optional=True))
FixBoardInfoDto.add_field('Name', StringField(field_number=10, optional=True))
FixBoardInfoDto.add_field('ShortName', StringField(field_number=11, optional=True))
FixBoardInfoDto.add_field('TotalTrade', DoubleField(field_number=12, optional=True, default=0))
FixBoardInfoDto.add_field('TotalStock', StringField(field_number=13, optional=True))
FixBoardInfoDto.add_field('NumSymbolAdvances', StringField(field_number=14, optional=True))
FixBoardInfoDto.add_field('NumSymbolNochange', StringField(field_number=15, optional=True))
FixBoardInfoDto.add_field('NumSymbolDeclines', StringField(field_number=16, optional=True))
FixBoardInfoDto.add_field('DateNo', StringField(field_number=17, optional=True))
FixBoardInfoDto.add_field('TotalNormalTradedQttyRd', DoubleField(field_number=18, optional=True, default=0))
FixBoardInfoDto.add_field('TotalNormalTradedValueRd', DoubleField(field_number=19, optional=True, default=0))
FixBoardInfoDto.add_field('TotalNormalTradedQttyOd', DoubleField(field_number=20, optional=True, default=0))
FixBoardInfoDto.add_field('TotalNormalTradedValueOd', DoubleField(field_number=21, optional=True, default=0))
FixBoardInfoDto.add_field('TotalPtTradedQtty', DoubleField(field_number=22, optional=True, default=0))
FixBoardInfoDto.add_field('TotalPtTradedValue', DoubleField(field_number=23, optional=True, default=0))
List_FixBoardInfoDto.add_field('items', MessageField(field_number=1, repeated=True, message_cls=FixBoardInfoDto))
List_MarketDataDto.add_field('items', MessageField(field_number=1, repeated=True, message_cls=MarketDataDto))
MarketDataDto.add_field('StockType', StringField(field_number=1, optional=True))
MarketDataDto.add_field('TradingDate', StringField(field_number=2, optional=True))
MarketDataDto.add_field('TradingTime', StringField(field_number=3, optional=True))
MarketDataDto.add_field('Exchange', StringField(field_number=4, optional=True))
MarketDataDto.add_field('ConfirmNo', StringField(field_number=5, optional=True))
MarketDataDto.add_field('StockNo', StringField(field_number=6, optional=True))
MarketDataDto.add_field('StockSymbol', StringField(field_number=7, optional=True))
MarketDataDto.add_field('TotalVol', StringField(field_number=8, optional=True))
MarketDataDto.add_field('TotalPrice', StringField(field_number=9, optional=True))
MarketDataDto.add_field('Side', StringField(field_number=10, optional=True))
MarketDataDto.add_field('AccumulatedVol', StringField(field_number=11, optional=True))
MarketDataDto.add_field('AccumulatedVal', StringField(field_number=12, optional=True))
MarketDataDto.add_field('Highest', StringField(field_number=13, optional=True))
MarketDataDto.add_field('Lowest', StringField(field_number=14, optional=True))
MarketDataDto.add_field('PutVolumn', StringField(field_number=15, optional=True))
MarketDataDto.add_field('PutPrice', StringField(field_number=16, optional=True))
AmqDto.add_field('ObjectType', StringField(field_number=1, optional=True))
AmqDto.add_field('Data', BytesField(field_number=2, optional=True))
List_AmqDto.add_field('items', MessageField(field_number=1, repeated=True, message_cls=AmqDto))
