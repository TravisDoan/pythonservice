class StocksInfoUpcom:
    def __init__(self, stockInfoID, tradingDate, time, stockID, code, stockType, totalListingQtty, tradingUnit,
                 listtingStatus, adjustQtty, referenceStatus, adjustRate, dividentRate, status, totalRoom, currentRoom,
                 basicPrice, openPrice, closePrice, currentPrice, currentQtty, highestPrice, lowestPrice,
                 bestOfferPrice, bestBidPrice, ceilingPrice, floorPrice, totalOfferQtty, totalBidQtty, bestOfferQtty,
                 bestBidQtty, priorPrice, priorClosePrice, matchPrice, matchQtty, deleted, dateCreated, dateModified,
                 modifiedBy, createdBy, name, parValue, floorCode, matchValue, isCalcIndex, isDeterminecl, dateNo,
                 offerCount, bidCount, averagePrice, indexPrice, prevPriorPrice, yieldmat, prevPriorClosePrice,
                 nmTotalTradedQtty, nmTotalTradedValue, ptMatchQtty, ptMatchPrice, ptTotalTradedQtty,
                 ptTotalTradedValue, totalBuyTradingQtty, buyCount, totalBuyTradingValue, totalSellTradingQtty,
                 sellCount, totalSellTradingValue, totalTradingQtty, totalTradingValue, buyForeignQtty, buyForeignValue,
                 sellForeignQtty, sellForeignValue, remainForeignQtty, ptYieldmat, dateReport, closePriceAdjusted,
                 rateAdjusted, highestAdjusted, lowestAdjusted, openPriceAdjusted, ceilingRate, floorRate,
                 isPrivateRateUsed, totalCustQtty, auMatchQtty, auMatchPrice, auTotalTradedQtty, auTotalTradedValue):
        self.stockInfoID = stockInfoID
        self.tradingDate = tradingDate
        self.time = time
        self.stockID = stockID
        self.code = code
        self.stockType = stockType
        self.totalListingQtty = totalListingQtty
        self.tradingUnit = tradingUnit
        self.listtingStatus = listtingStatus
        self.adjustQtty = adjustQtty
        self.referenceStatus = referenceStatus
        self.adjustRate = adjustRate
        self.dividentRate = dividentRate
        self.status = status
        self.totalRoom = totalRoom
        self.currentRoom = currentRoom
        self.basicPrice = basicPrice
        self.openPrice = openPrice
        self.closePrice = closePrice
        self.currentPrice = currentPrice
        self.currentQtty = currentQtty
        self.highestPrice = highestPrice
        self.lowestPrice = lowestPrice
        self.bestOfferPrice = bestOfferPrice
        self.bestBidPrice = bestBidPrice
        self.ceilingPrice = ceilingPrice
        self.floorPrice = floorPrice
        self.totalOfferQtty = totalOfferQtty
        self.totalBidQtty = totalBidQtty
        self.bestOfferQtty = bestOfferQtty
        self.bestBidQtty = bestBidQtty
        self.priorPrice = priorPrice
        self.priorClosePrice = priorClosePrice
        self.matchPrice = matchPrice
        self.matchQtty = matchQtty
        self.deleted = deleted
        self.dateCreated = dateCreated
        self.dateModified = dateModified
        self.modifiedBy = modifiedBy
        self.createdBy = createdBy
        self.name = name
        self.parValue = parValue
        self.floorCode = floorCode
        self.matchValue = matchValue
        self.isCalcIndex = isCalcIndex
        self.isDeterminecl = isDeterminecl
        self.dateNo = dateNo
        self.offerCount = offerCount
        self.bidCount = bidCount
        self.averagePrice = averagePrice
        self.indexPrice = indexPrice
        self.prevPriorPrice = prevPriorPrice
        self.yieldmat = yieldmat
        self.prevPriorClosePrice = prevPriorClosePrice
        self.nmTotalTradedQtty = nmTotalTradedQtty
        self.nmTotalTradedValue = nmTotalTradedValue
        self.ptMatchQtty = ptMatchQtty
        self.ptMatchPrice = ptMatchPrice
        self.ptTotalTradedQtty = ptTotalTradedQtty
        self.ptTotalTradedValue = ptTotalTradedValue
        self.totalBuyTradingQtty = totalBuyTradingQtty
        self.buyCount = buyCount
        self.totalBuyTradingValue = totalBuyTradingValue
        self.totalSellTradingQtty = totalSellTradingQtty
        self.sellCount = sellCount
        self.totalSellTradingValue = totalSellTradingValue
        self.totalTradingQtty = totalTradingQtty
        self.totalTradingValue = totalTradingValue
        self.buyForeignQtty = buyForeignQtty
        self.buyForeignValue = buyForeignValue
        self.sellForeignQtty = sellForeignQtty
        self.sellForeignValue = sellForeignValue
        self.remainForeignQtty = remainForeignQtty
        self.ptYieldmat = ptYieldmat
        self.dateReport = dateReport
        self.closePriceAdjusted = closePriceAdjusted
        self.rateAdjusted = rateAdjusted
        self.highestAdjusted = highestAdjusted
        self.lowestAdjusted = lowestAdjusted
        self.openPriceAdjusted = openPriceAdjusted
        self.ceilingRate = ceilingRate
        self.floorRate = floorRate
        self.isPrivateRateUsed = isPrivateRateUsed
        self.totalCustQtty = totalCustQtty
        self.auMatchQtty = auMatchQtty
        self.auMatchPrice = auMatchPrice
        self.auTotalTradedQtty = auTotalTradedQtty
        self.auTotalTradedValue = auTotalTradedValue


class MarketsInfoUpCom:
    def __init__(self, marketInfoID, floorCode, dateNo, tradingDate, currentStatus, time, totalTrade, totalStock,
                 advances, declines, nochange, totalQtty, totalValue, priorMarketIndex, chgIndex, pctIndex,
                 currentIndex, marketIndex, sessionNo, lowest, highest, prvPriorMarketIndex, avrMarketIndex,
                 avrPriorMarketIndex, avrChgIndex, avrPctIndex, ptTotalQtty, ptTotalValue, ptTotalTrade, dateReport,
                 openIndex):
        self.marketInfoID = marketInfoID
        self.floorCode = floorCode
        self.dateNo = dateNo
        self.tradingDate = tradingDate
        self.currentStatus = currentStatus
        self.time = time
        self.totalTrade = totalTrade
        self.totalStock = totalStock
        self.advances = advances
        self.declines = declines
        self.nochange = nochange
        self.totalQtty = totalQtty
        self.totalValue = totalValue
        self.priorMarketIndex = priorMarketIndex
        self.chgIndex = chgIndex
        self.pctIndex = pctIndex
        self.currentIndex = currentIndex
        self.marketIndex = marketIndex
        self.sessionNo = sessionNo
        self.lowest = lowest
        self.highest = highest
        self.prvPriorMarketIndex = prvPriorMarketIndex
        self.avrMarketIndex = avrMarketIndex
        self.avrPriorMarketIndex = avrPriorMarketIndex
        self.avrChgIndex = avrChgIndex
        self.avrPctIndex = avrPctIndex
        self.ptTotalQtty = ptTotalQtty
        self.ptTotalValue = ptTotalValue
        self.ptTotalTrade = ptTotalTrade
        self.dateReport = dateReport
        self.openIndex = openIndex
