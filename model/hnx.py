class StocksInfoHNX:
    def __init__(self, tradingDate, symbolID, symbol, boardCode, boardStatus, securityType, tradingSessionID,
                 tradSesStatus, stockType, tradingUnit, listingStatus, adjustQTTY, referenceStatus,
                 adjustRate, dividentRate, securityTradingStatus, openPrice, closePrice, currentPrice,
                 currentQTTY, highestPrice, lowestPrice, bestOfferPrice, bestBidPrice, ceilingPrice, floorPrice,
                 totalOfferQTTY, totalBidQTTY, totalBidQTTYOD, totalOfferQTTYOD, bestOfferQTTY, bestBidQTTY, priorPrice,
                 priorClosePrice, matchPrice, matchQTTY, deleted, dateCreated, dateModified, modifiedBy, createdBy,
                 name, shortName, parValue, marketCode, dateNo, offerCount, bidCount, nmTotalTradedQTTY,
                 nmTotalTradedValue, ptMatchQTTY, ptMatchPrice, ptTotalTradedQTTY, ptTotalTradedValue,
                 totalBuyTradingQTTY, buyCount, totalBuyTradingValue, totalSellTradingQTTY, sellCount,
                 totalSellTradingValue, totalVolumeTraded, totalValueTraded, tradingTime, issuerName, issueDate,
                 maturityDate, couponRate, securityDesc, buyForeignQTTY, buyForeignValue, sellForeignQTTY,
                 sellForeignValue, remainForeignQTTY, totalStock, totalTrade, numSymbolAdvances, numSymbolNoChange,
                 numSymbolDeclines, totalNormalTradedQTTYRD, totalNormalTradedValueRD, totalNormalTradedQTTYOd,
                 totalNormalTradedValueOd, totalPTTradedQTTY, totalPTTradedValue, underlyingSymbol, firstTradingDate,
                 lastTradingDate, coveredWarrantType, exercisePrice, exerciseRatio, listedShare, openInterest,
                 openInterestChange, secSession, cAStatus, refPrice, avgPrice, best1Bid, best2Bid, best3Bid, best4Bid,
                 best5Bid, best6Bid, best7Bid, best8Bid, best9Bid, best10Bid, best1BidVol, best2BidVol, best3BidVol,
                 best4BidVol, best5BidVol, best6BidVol, best7BidVol, best8BidVol, best9BidVol, best10BidVol, best1Offer,
                 best2Offer, best3Offer, best4Offer, best5Offer, best6Offer, best7Offer, best8Offer, best9Offer,
                 best10Offer, best1OfferVol, best2OfferVol, best3OfferVol, best4OfferVol, best5OfferVol, best6OfferVol,
                 best7OfferVol, best8OfferVol, best9OfferVol, best10OfferVol, priceChange, priceChangePercentage):
        self.tradingDate = tradingDate
        self.symbolID = symbolID
        self.symbol = symbol
        self.boardCode = boardCode
        self.boardStatus = boardStatus
        self.securityType = securityType
        self.tradingSessionID = tradingSessionID
        self.tradSesStatus = tradSesStatus
        self.stockType = stockType
        self.tradingUnit = tradingUnit
        self.listingStatus = listingStatus
        self.adjustQTTY = adjustQTTY
        self.referenceStatus = referenceStatus
        self.adjustRate = adjustRate
        self.dividentRate = dividentRate
        self.securityTradingStatus = securityTradingStatus
        self.openPrice = openPrice
        self.closePrice = closePrice
        self.currentPrice = currentPrice
        self.currentQTTY = currentQTTY
        self.highestPrice = highestPrice
        self.lowestPrice = lowestPrice
        self.bestOfferPrice = bestOfferPrice
        self.bestBidPrice = bestBidPrice
        self.ceilingPrice = ceilingPrice
        self.floorPrice = floorPrice
        self.totalOfferQTTY = totalOfferQTTY
        self.totalBidQTTY = totalBidQTTY
        self.totalBidQTTYOD = totalBidQTTYOD
        self.totalOfferQTTYOD = totalOfferQTTYOD
        self.bestOfferQTTY = bestOfferQTTY
        self.bestBidQTTY = bestBidQTTY
        self.priorPrice = priorPrice
        self.priorClosePrice = priorClosePrice
        self.matchPrice = matchPrice
        self.matchQTTY = matchQTTY
        self.deleted = deleted
        self.dateCreated = dateCreated
        self.dateModified = dateModified
        self.modifiedBy = modifiedBy
        self.createdBy = createdBy
        self.name = name
        self.shortName = shortName
        self.parValue = parValue
        self.marketCode = marketCode
        self.dateNo = dateNo
        self.offerCount = offerCount
        self.bidCount = bidCount
        self.nmTotalTradedQTTY = nmTotalTradedQTTY
        self.nmTotalTradedValue = nmTotalTradedValue
        self.ptMatchQTTY = ptMatchQTTY
        self.ptMatchPrice = ptMatchPrice
        self.ptTotalTradedQTTY = ptTotalTradedQTTY
        self.ptTotalTradedValue = ptTotalTradedValue
        self.totalBuyTradingQTTY = totalBuyTradingQTTY
        self.buyCount = buyCount
        self.totalBuyTradingValue = totalBuyTradingValue
        self.totalSellTradingQTTY = totalSellTradingQTTY
        self.sellCount = sellCount
        self.totalSellTradingValue = totalSellTradingValue
        self.totalVolumeTraded = totalVolumeTraded
        self.totalValueTraded = totalValueTraded
        self.tradingTime = tradingTime
        self.issuerName = issuerName
        self.issueDate = issueDate
        self.maturityDate = maturityDate
        self.couponRate = couponRate
        self.securityDesc = securityDesc
        self.buyForeignQTTY = buyForeignQTTY
        self.buyForeignValue = buyForeignValue
        self.sellForeignQTTY = sellForeignQTTY
        self.sellForeignValue = sellForeignValue
        self.remainForeignQTTY = remainForeignQTTY
        self.totalStock = totalStock
        self.totalTrade = totalTrade
        self.numSymbolAdvances = numSymbolAdvances
        self.numSymbolNoChange = numSymbolNoChange
        self.numSymbolDeclines = numSymbolDeclines
        self.totalNormalTradedQTTYRD = totalNormalTradedQTTYRD
        self.totalNormalTradedValueRD = totalNormalTradedValueRD
        self.totalNormalTradedQTTYOd = totalNormalTradedQTTYOd
        self.totalNormalTradedValueOd = totalNormalTradedValueOd
        self.totalPTTradedQTTY = totalPTTradedQTTY
        self.totalPTTradedValue = totalPTTradedValue
        self.underlyingSymbol = underlyingSymbol
        self.firstTradingDate = firstTradingDate
        self.lastTradingDate = lastTradingDate
        self.coveredWarrantType = coveredWarrantType
        self.exercisePrice = exercisePrice
        self.exerciseRatio = exerciseRatio
        self.listedShare = listedShare
        self.openInterest = openInterest
        self.openInterestChange = openInterestChange
        self.secSession = secSession
        self.cAStatus = cAStatus
        self.refPrice = refPrice
        self.avgPrice = avgPrice
        self.best1Bid = best1Bid
        self.best2Bid = best2Bid
        self.best3Bid = best3Bid
        self.best4Bid = best4Bid
        self.best5Bid = best5Bid
        self.best6Bid = best6Bid
        self.best7Bid = best7Bid
        self.best8Bid = best8Bid
        self.best9Bid = best9Bid
        self.best10Bid = best10Bid
        self.best1BidVol = best1BidVol
        self.best2BidVol = best2BidVol
        self.best3BidVol = best3BidVol
        self.best4BidVol = best4BidVol
        self.best5BidVol = best5BidVol
        self.best6BidVol = best6BidVol
        self.best7BidVol = best7BidVol
        self.best8BidVol = best8BidVol
        self.best9BidVol = best9BidVol
        self.best10BidVol = best10BidVol
        self.best1Offer = best1Offer
        self.best2Offer = best2Offer
        self.best3Offer = best3Offer
        self.best4Offer = best4Offer
        self.best5Offer = best5Offer
        self.best6Offer = best6Offer
        self.best7Offer = best7Offer
        self.best8Offer = best8Offer
        self.best9Offer = best9Offer
        self.best10Offer = best10Offer
        self.best1OfferVol = best1OfferVol
        self.best2OfferVol = best2OfferVol
        self.best3OfferVol = best3OfferVol
        self.best4OfferVol = best4OfferVol
        self.best5OfferVol = best5OfferVol
        self.best6OfferVol = best6OfferVol
        self.best7OfferVol = best7OfferVol
        self.best8OfferVol = best8OfferVol
        self.best9OfferVol = best9OfferVol
        self.best10OfferVol = best10OfferVol
        self.priceChange = priceChange
        self.priceChangePercentage = priceChangePercentage


class MarketsInfoHNX:

    def __init__(self, indexID, indexCode, indexValue, calTime, change, ratioChange, totalQTTY, totalValue, tradingDate,
                 currentStatus, totalStock, priorIndexVal, highestIndex, lowestIndex, closeIndex, typeIndex, indexName,
                 weighted, addDate, symbolID, symbol, advances, noChanges, declines, ceiling, floor,
                 totalNormalTradedQTTYOD, totalNormalTradedValueOd, totalPTTradedQTTY, totalPTTradedValue):
        self.indexID = indexID
        self.indexCode = indexCode
        self.indexValue = indexValue
        self.calTime = calTime
        self.change = change
        self.ratioChange = ratioChange
        self.totalQTTY = totalQTTY
        self.totalValue = totalValue
        self.tradingDate = tradingDate
        self.currentStatus = currentStatus
        self.totalStock = totalStock
        self.priorIndexVal = priorIndexVal
        self.highestIndex = highestIndex
        self.lowestIndex = lowestIndex
        self.closeIndex = closeIndex
        self.typeIndex = typeIndex
        self.indexName = indexName
        self.weighted = weighted
        self.addDate = addDate
        self.symbolID = symbolID
        self.symbol = symbol
        self.advances = advances
        self.noChanges = noChanges
        self.declines = declines
        self.ceiling = ceiling
        self.floor = floor
        self.totalNormalTradedQTTYOD = totalNormalTradedQTTYOD
        self.totalNormalTradedValueOd = totalNormalTradedValueOd
        self.totalPTTradedQTTY = totalPTTradedQTTY
        self.totalPTTradedValue = totalPTTradedValue
