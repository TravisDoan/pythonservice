from model import singletonthread, stockinfo, temp
from constants import config
from service import stock
from datetime import date, datetime

import json

class ProjsonClient(metaclass=singletonthread.Singleton):
    pass

    stockService = stock.StockService()

    def __init__(self):
        self.ping = 'pong'

    def initSerialize(self, jsonStr):
        jsonObj = json.loads(jsonStr)
        for itemJs in jsonObj:
            tmp = temp.HNXTemp()
            print("child", itemJs)
            if itemJs['ObjectType'] == config.FIX_INDEX_DTO:
                indexDto = json.loads(itemJs['Data'])
                print(indexDto)
                if indexDto['IndexId'] == '' or indexDto['IndexId'] is None:
                    tmp.indexId = None
                else:
                    tmp.indexId = indexDto['IndexId']
                if indexDto['IndexValue'] == '' or indexDto['IndexValue'] is None:
                    tmp.indexValue = None
                else:
                    tmp.indexValue = indexDto['IndexValue']
                if indexDto['TradingDate'] == '' or indexDto['TradingDate'] is None:
                    tmp.tradingDate = None
                else:
                    tradingDateStr = indexDto['TradingDate']
                    tmp.tradingDate = datetime.strptime(tradingDateStr.replace("/", ""), "%d%m%Y").date()
                if indexDto['Time'] == '' or indexDto['Time'] is None:
                    tmp.calTime = None
                else:
                    tmp.calTime = indexDto['Time']
                if indexDto['Change'] == '' or indexDto['Change'] is None:
                    tmp.change = None
                else:
                    tmp.change = indexDto['Change']
                if indexDto['RatioChange'] == '' or indexDto['RatioChange'] is None:
                    tmp.ratioChange = None
                else:
                    tmp.ratioChange = indexDto['RatioChange']
                if indexDto['TotalTrade'] == '' or indexDto['TotalTrade'] is None:
                    tmp.totalTrade = None
                else:
                    tmp.totalTrade = indexDto['TotalTrade']
                if indexDto['TotalQtty'] == '' or indexDto['TotalQtty'] is None:
                    tmp.totalQtty = None
                else:
                    tmp.totalQtty = indexDto['TotalQtty']
                if indexDto['TotalValue'] == '' or indexDto['TotalValue'] is None:
                    tmp.totalValue = None
                else:
                    tmp.totalValue = indexDto['TotalValue']
                if indexDto['TypeIndex'] == '' or indexDto['TypeIndex'] is None:
                    tmp.typeIndex = None
                else:
                    tmp.typeIndex = indexDto['TypeIndex']
                if indexDto['IndexName'] == '' or indexDto['IndexName'] is None:
                    tmp.indexName = None
                else:
                    tmp.indexName = indexDto['IndexName']
                if indexDto['Advances'] == '' or indexDto['Advances'] is None:
                    tmp.advances = None
                else:
                    tmp.advances = indexDto['Advances']
                if indexDto['Nochanges'] == '' or indexDto['Nochanges'] is None:
                    tmp.nochanges = None
                else:
                    tmp.nochanges = indexDto['Nochanges']
                if indexDto['Declines'] == '' or indexDto['Declines'] is None:
                    tmp.declines = None
                else:
                    tmp.declines = indexDto['Declines']
                if indexDto['Ceiling'] == '' or indexDto['Ceiling'] is None:
                    tmp.ceilingPrice = None
                else:
                    tmp.ceilingPrice = int(indexDto['Ceiling'])
                if indexDto['Floor'] == '' or indexDto['Floor'] is None:
                    tmp.floorPrice = None
                else:
                    tmp.floorPrice = int(indexDto['Floor'])
                if indexDto['IndexTotalStock'] == '' or indexDto['IndexTotalStock'] is None:
                    tmp.indexTotalStock = None
                else:
                    tmp.indexTotalStock = indexDto['IndexTotalStock']
                if indexDto['PriorIndexVal'] == '' or indexDto['PriorIndexVal'] is None:
                    tmp.priorIndexVal = None
                else:
                    tmp.priorIndexVal = indexDto['PriorIndexVal']
                if indexDto['HighestIndex'] == '' or indexDto['HighestIndex'] is None:
                    tmp.highestPriceIndex = None
                else:
                    tmp.highestPriceIndex = indexDto['HighestIndex']
                if indexDto['LowestIndex'] == '' or indexDto['LowestIndex'] is None:
                    tmp.lowestPriceIndex = None
                else:
                    tmp.lowestPriceIndex = indexDto['LowestIndex']
                if indexDto['CloseIndex'] == '' or indexDto['CloseIndex'] is None:
                    tmp.closeIndex = None
                else:
                    tmp.closeIndex = indexDto['CloseIndex']
                if indexDto['MsgType'] == '' or indexDto['MsgType'] is None:
                    tmp.msgType = None
                else:
                    tmp.msgType = indexDto['MsgType']
                # boardInfo
                tmp.boardCode = None
                tmp.marketCode = None
                tmp.session = None
                tmp.tradingSessionID = None
                tmp.tradeStatus = None
                tmp.time = None
                tmp.secSessionCode = None
                tmp.secSessionStatus = None
                tmp.msgType = None
                tmp.boardStatus = None
                tmp.name = None
                tmp.shortName = None
                tmp.totalTrade = None
                tmp.totalStock = None
                tmp.numSymbolAdvances = None
                tmp.numSymbolNoChange = None
                tmp.numSymbolDeclines = None
                tmp.dateNo = None
                tmp.totalNormalTradedQTTYRD = None
                tmp.totalNormalTradedValueRD = None
                tmp.totalNormalTradedQTTYOd = None
                tmp.totalNormalTradedValueOd = None
                tmp.totalPTTradedQTTY = None
                tmp.totalPTTradedValue = None
                # securityInfo
                tmp.tradingDate = None
                tmp.tradingTime = None
                # tmp.boardCode = None
                tmp.marketCode = None
                tmp.symbolID = None
                tmp.symbol = None
                tmp.securityType = None
                tmp.securityName = None
                tmp.issueDate = None
                tmp.tradingUnit = None
                tmp.parValue = None
                tmp.underlyingSymbol = None
                tmp.issuerName = None
                tmp.coveredWarrantType = None
                tmp.maturityDate = None
                tmp.firstTradingDate = None
                tmp.lastTradingDate = None
                tmp.exercisePrice = None
                tmp.exerciseRatio = None
                tmp.listedShare = None
                tmp.openInterest = None
                tmp.openInterestChange = None
                tmp.secSessionCode = None
                tmp.tradingStatus = None
                tmp.securityTradingStatus = None
                tmp.cAStatus = None
                tmp.referenceStatus = None
                tmp.refPrice = None
                tmp.ceilingPrice = None
                tmp.floorPrice = None
                tmp.floorPricePricePt = None
                tmp.ceilingPricePricePt = None
                tmp.matchPrice = None
                tmp.matchQTTY = None
                tmp.priceChange = None
                tmp.priceChangePercentage = None
                tmp.priorClosePrice = None
                tmp.priorCloseDate = None
                tmp.highestPrice = None
                tmp.avgPrice = None
                tmp.lowestPrice = None
                tmp.offerCount = None
                tmp.totalBidQty = None
                tmp.totalOfferQty = None
                tmp.bidCount = None
                tmp.nmTotalTradedQTTY = None
                tmp.nmTotalTradedValue = None
                tmp.ptMatchQTTY = None
                tmp.ptMatchPrice = None
                tmp.ptTotalTradedQTTY = None
                tmp.ptTotalTradedValue = None
                tmp.totalBuyTradingQTTY = None
                tmp.buyCount = None
                tmp.totalBuyTradingValue = None
                tmp.totalSellTradingQTTY = None
                tmp.sellCount = None
                tmp.totalSellTradingValue = None
                tmp.buyForeignQTTY = None
                tmp.buyForeignValue = None
                tmp.sellForeignQTTY = None
                tmp.sellForeignValue = None
                tmp.remainForeignQTTY = None
                tmp.totalBidQTTYOD = None
                tmp.totalOfferQTTYOD = None
                tmp.secSessionStatus = None
                tmp.putPrice = None
                tmp.putVolumn = None
                tmp.msgType = None
                tmp.secSession = None
                # other
                tmp.weighted = None
                tmp.addDate = None
                # tmp.symbol = None
                # tmp.symbolID = None
                tmp.currentStatus = None
                tmp.indexCode = None
                tmp.listingStatus = None
                tmp.openPrice = None
                tmp.closePrice = None
                tmp.currentPrice = None
                tmp.currentQTTY = None
                tmp.bestOfferPrice = None
                tmp.bestBidPrice = None
                tmp.bestOfferQTTY = None
                tmp.bestBidQTTY = None
                tmp.priorPrice = None
                tmp.totalVolumeTraded = None
                tmp.totalValueTraded = None
                tmp.couponRate = None
                tmp.securityDesc = None
                tmp.tradSesStatus = None
                tmp.stockType = None
                tmp.adjustQTTY = None
                tmp.adjustRate = None
                tmp.dividentRate = None
                tmp.totalOfferQTTY = None
                tmp.totalBidQTTY = None
                # tmp.matchPrice = None
                # tmp.matchQTTY = None
                tmp.deleted = None
                tmp.dateCreated = date.today()
                tmp.dateModified = date.today()
                tmp.modifiedBy = "Admin"
                tmp.createdBy = "Admin"
                tmp.best1Bid = None
                tmp.best2Bid = None
                tmp.best3Bid = None
                tmp.best4Bid = None
                tmp.best5Bid = None
                tmp.best6Bid = None
                tmp.best7Bid = None
                tmp.best8Bid = None
                tmp.best9Bid = None
                tmp.best10Bid = None
                tmp.best1BidVol = None
                tmp.best2BidVol = None
                tmp.best3BidVol = None
                tmp.best4BidVol = None
                tmp.best5BidVol = None
                tmp.best6BidVol = None
                tmp.best7BidVol = None
                tmp.best8BidVol = None
                tmp.best9BidVol = None
                tmp.best10BidVol = None
                tmp.best1Offer = None
                tmp.best2Offer = None
                tmp.best3Offer = None
                tmp.best4Offer = None
                tmp.best5Offer = None
                tmp.best6Offer = None
                tmp.best7Offer = None
                tmp.best8Offer = None
                tmp.best9Offer = None
                tmp.best10Offer = None
                tmp.best1OfferVol = None
                tmp.best2OfferVol = None
                tmp.best3OfferVol = None
                tmp.best4OfferVol = None
                tmp.best5OfferVol = None
                tmp.best6OfferVol = None
                tmp.best7OfferVol = None
                tmp.best8OfferVol = None
                tmp.best9OfferVol = None
                tmp.best10OfferVol = None
            elif itemJs['ObjectType'] == config.FIX_BOARD_INFO_DTO:
                boardDto = json.loads(itemJs['Data'])
                if boardDto['Exchange'] == '' or boardDto['Exchange'] is None:
                    tmp.boardCode = None
                else:
                    tmp.boardCode = boardDto['Exchange']
                if boardDto['Exchange'] == '' or boardDto['Exchange'] is None:
                    tmp.marketCode = None
                else:
                    tmp.marketCode = boardDto['Exchange']
                if boardDto['Session'] == '' or boardDto['Session'] is None:
                    tmp.session = None
                else:
                    tmp.session = boardDto['Session']
                if boardDto['TradingSession'] == '' or boardDto['TradingSession'] is None:
                    tmp.tradingSessionID = None
                else:
                    tmp.tradingSessionID = boardDto['TradingSession']
                if boardDto['TradeStatus'] == '' or boardDto['TradeStatus'] is None:
                    tmp.tradeStatus = None
                else:
                    tmp.tradeStatus = boardDto['TradeStatus']
                if boardDto['Time'] == '' or boardDto['Time'] is None:
                    tmp.time = None
                else:
                    tmp.time = boardDto['Time']
                if boardDto['SecSessionCode'] == '' or boardDto['SecSessionCode'] is None:
                    tmp.secSessionCode = None
                else:
                    tmp.secSessionCode = boardDto['SecSessionCode']
                if boardDto['SecSessionStatus'] == '' or boardDto['SecSessionStatus'] is None:
                    tmp.secSessionStatus = None
                else:
                    tmp.secSessionStatus = boardDto['SecSessionStatus']
                if boardDto['MsgType'] == '' or boardDto['MsgType'] is None:
                    tmp.msgType = None
                else:
                    tmp.msgType = boardDto['MsgType']
                if boardDto['BoardStatus'] == '' or boardDto['BoardStatus'] is None:
                    tmp.boardStatus = None
                else:
                    tmp.boardStatus = boardDto['BoardStatus']
                if boardDto['Name'] == '' or boardDto['Name'] is None:
                    tmp.name = None
                else:
                    tmp.name = boardDto['Name']
                if boardDto['ShortName'] == '' or boardDto['ShortName'] is None:
                    tmp.shortName = None
                else:
                    tmp.shortName = boardDto['ShortName']
                if boardDto['TotalTrade'] == '' or boardDto['TotalTrade'] is None:
                    tmp.totalTrade = None
                else:
                    tmp.totalTrade = float(boardDto['TotalTrade'])
                if boardDto['TotalStock'] == '' or boardDto['TotalStock'] is None:
                    tmp.totalStock = None
                else:
                    tmp.totalStock = int(boardDto['TotalStock'])
                if boardDto['NumSymbolAdvances'] == '' or boardDto['NumSymbolAdvances'] is None:
                    tmp.numSymbolAdvances = None
                else:
                    tmp.numSymbolAdvances = int(boardDto['NumSymbolAdvances'])
                if boardDto['NumSymbolNochange'] == '' or boardDto['NumSymbolNochange'] is None:
                    tmp.numSymbolNoChange = None
                else:
                    tmp.numSymbolNoChange = int(boardDto['NumSymbolNochange'])
                if boardDto['NumSymbolDeclines'] == '' or boardDto['NumSymbolDeclines'] is None:
                    tmp.numSymbolDeclines = None
                else:
                    tmp.numSymbolDeclines = int(boardDto['NumSymbolDeclines'])
                if boardDto['DateNo'] == '' or boardDto['DateNo'] is None:
                    tmp.dateNo = None
                else:
                    tmp.dateNo = int(boardDto['DateNo'])
                if boardDto['TotalNormalTradedQttyRd'] == '' or boardDto['TotalNormalTradedQttyRd'] is None:
                    tmp.totalNormalTradedQTTYRD = None
                else:
                    tmp.totalNormalTradedQTTYRD = float(boardDto['TotalNormalTradedQttyRd'])
                if boardDto['TotalNormalTradedValueRd'] == '' or boardDto['TotalNormalTradedValueRd'] is None:
                    tmp.totalNormalTradedValueRD = None
                else:
                    tmp.totalNormalTradedValueRD = float(boardDto['TotalNormalTradedValueRd'])
                if boardDto['TotalNormalTradedQttyOd'] == '' or boardDto['TotalNormalTradedQttyOd'] is None:
                    tmp.totalNormalTradedQTTYOd = None
                else:
                    tmp.totalNormalTradedQTTYOd = float(boardDto['TotalNormalTradedQttyOd'])
                if boardDto['TotalNormalTradedValueOd'] == '' or boardDto['TotalNormalTradedValueOd'] is None:
                    tmp.totalNormalTradedValueOd = None
                else:
                    tmp.totalNormalTradedValueOd = float(boardDto['TotalNormalTradedValueOd'])
                if boardDto['TotalPtTradedQtty'] == '' or boardDto['TotalPtTradedQtty'] is None:
                    tmp.totalPTTradedQTTY = None
                else:
                    tmp.totalPTTradedQTTY = float(boardDto['TotalPtTradedQtty'])
                if boardDto['TotalPtTradedValue'] == '' or boardDto['TotalPtTradedValue'] is None:
                    tmp.totalPTTradedValue = None
                else:
                    tmp.totalPTTradedValue = float(boardDto['TotalPtTradedValue'])
                # indexInfo
                tmp.indexId = None
                tmp.indexValue = None
                tmp.tradingDate = None
                tmp.calTime = None
                tmp.change = None
                tmp.ratioChange = None
                tmp.totalTrade = None
                tmp.totalQtty = None
                tmp.totalValue = None
                tmp.typeIndex = None
                tmp.indexName = None
                tmp.advances = None
                tmp.nochanges = None
                tmp.declines = None
                tmp.ceilingPrice = None
                tmp.floorPrice = None
                tmp.indexTotalStock = None
                tmp.priorIndexVal = None
                tmp.highestPriceIndex = None
                tmp.lowestPriceIndex = None
                tmp.closeIndex = None
                tmp.msgType = None
                # securityInfo
                tmp.tradingDate = None
                tmp.tradingTime = None
                # tmp.boardCode = None
                # tmp.marketCode = None
                # tmp.symbolID = None
                # tmp.symbol = None
                tmp.securityType = None
                tmp.securityName = None
                tmp.issueDate = None
                tmp.tradingUnit = None
                tmp.parValue = None
                tmp.underlyingSymbol = None
                tmp.issuerName = None
                tmp.coveredWarrantType = None
                tmp.maturityDate = None
                tmp.firstTradingDate = None
                tmp.lastTradingDate = None
                tmp.exercisePrice = None
                tmp.exerciseRatio = None
                tmp.listedShare = None
                tmp.openInterest = None
                tmp.openInterestChange = None
                tmp.secSessionCode = None
                tmp.tradingStatus = None
                tmp.securityTradingStatus = None
                tmp.cAStatus = None
                tmp.referenceStatus = None
                tmp.refPrice = None
                tmp.ceilingPrice = None
                tmp.floorPrice = None
                tmp.floorPricePricePt = None
                tmp.ceilingPricePricePt = None
                tmp.matchPrice = None
                tmp.matchQTTY = None
                tmp.priceChange = None
                tmp.priceChangePercentage = None
                tmp.priorClosePrice = None
                tmp.priorCloseDate = None
                tmp.highestPrice = None
                tmp.avgPrice = None
                tmp.lowestPrice = None
                tmp.offerCount = None
                tmp.totalBidQty = None
                tmp.totalOfferQty = None
                tmp.bidCount = None
                tmp.nmTotalTradedQTTY = None
                tmp.nmTotalTradedValue = None
                tmp.ptMatchQTTY = None
                tmp.ptMatchPrice = None
                tmp.ptTotalTradedQTTY = None
                tmp.ptTotalTradedValue = None
                tmp.totalBuyTradingQTTY = None
                tmp.buyCount = None
                tmp.totalBuyTradingValue = None
                tmp.totalSellTradingQTTY = None
                tmp.sellCount = None
                tmp.totalSellTradingValue = None
                tmp.buyForeignQTTY = None
                tmp.buyForeignValue = None
                tmp.sellForeignQTTY = None
                tmp.sellForeignValue = None
                tmp.remainForeignQTTY = None
                tmp.totalBidQTTYOD = None
                tmp.totalOfferQTTYOD = None
                # tmp.secSessionStatus = None
                tmp.putPrice = None
                tmp.putVolumn = None
                tmp.msgType = None
                tmp.secSession = None
                # other
                tmp.weighted = None
                tmp.addDate = None
                tmp.symbol = None
                tmp.symbolID = None
                tmp.currentStatus = None
                tmp.indexCode = None
                tmp.listingStatus = None
                tmp.openPrice = None
                tmp.closePrice = None
                tmp.currentPrice = None
                tmp.currentQTTY = None
                tmp.bestOfferPrice = None
                tmp.bestBidPrice = None
                tmp.bestOfferQTTY = None
                tmp.bestBidQTTY = None
                tmp.priorPrice = None
                tmp.totalVolumeTraded = None
                tmp.totalValueTraded = None
                tmp.couponRate = None
                tmp.securityDesc = None
                tmp.tradSesStatus = None
                tmp.stockType = None
                tmp.adjustQTTY = None
                tmp.adjustRate = None
                tmp.dividentRate = None
                tmp.totalOfferQTTY = None
                tmp.totalBidQTTY = None
                # tmp.matchPrice = None
                # tmp.matchQTTY = None
                tmp.deleted = None
                tmp.dateCreated = date.today()
                tmp.dateModified = date.today()
                tmp.modifiedBy = "Admin"
                tmp.createdBy = "Admin"
                tmp.best1Bid = None
                tmp.best2Bid = None
                tmp.best3Bid = None
                tmp.best4Bid = None
                tmp.best5Bid = None
                tmp.best6Bid = None
                tmp.best7Bid = None
                tmp.best8Bid = None
                tmp.best9Bid = None
                tmp.best10Bid = None
                tmp.best1BidVol = None
                tmp.best2BidVol = None
                tmp.best3BidVol = None
                tmp.best4BidVol = None
                tmp.best5BidVol = None
                tmp.best6BidVol = None
                tmp.best7BidVol = None
                tmp.best8BidVol = None
                tmp.best9BidVol = None
                tmp.best10BidVol = None
                tmp.best1Offer = None
                tmp.best2Offer = None
                tmp.best3Offer = None
                tmp.best4Offer = None
                tmp.best5Offer = None
                tmp.best6Offer = None
                tmp.best7Offer = None
                tmp.best8Offer = None
                tmp.best9Offer = None
                tmp.best10Offer = None
                tmp.best1OfferVol = None
                tmp.best2OfferVol = None
                tmp.best3OfferVol = None
                tmp.best4OfferVol = None
                tmp.best5OfferVol = None
                tmp.best6OfferVol = None
                tmp.best7OfferVol = None
                tmp.best8OfferVol = None
                tmp.best9OfferVol = None
                tmp.best10OfferVol = None
            elif itemJs['ObjectType'] == config.FIX_SECURITY_DTO:
                securityDto = json.loads(itemJs['Data'])
                if securityDto['TradingDate'] == '' or securityDto['TradingDate'] is None:
                    tmp.tradingDate = None
                else:
                    tradingDateStr = securityDto['TradingDate']
                    tmp.tradingDate = datetime.strptime(tradingDateStr.replace("/", ""), "%d%m%Y").date()
                if securityDto['TradingTime'] == '' or securityDto['TradingTime'] is None:
                    tmp.tradingTime = None
                else:
                    tmp.tradingTime = securityDto['TradingTime']
                if securityDto['Exchange'] == '' or securityDto['Exchange'] is None:
                    tmp.boardCode = None
                else:
                    tmp.boardCode = securityDto['Exchange']
                if securityDto['Exchange'] == '' or securityDto['Exchange'] is None:
                    tmp.marketCode = None
                else:
                    tmp.marketCode = securityDto['Exchange']
                if securityDto['StockNo'] == '' or securityDto['StockNo'] is None:
                    tmp.symbolID = None
                else:
                    tmp.symbolID = float(securityDto['StockNo'])
                if securityDto['Symbol'] == '' or securityDto['Symbol'] is None:
                    tmp.symbol = None
                else:
                    tmp.symbol = securityDto['Symbol']
                if securityDto['StockType'] == '' or securityDto['StockType'] is None:
                    tmp.securityType = None
                else:
                    tmp.securityType = securityDto['StockType']
                if securityDto['SecurityName'] == '' or securityDto['SecurityName'] is None:
                    tmp.securityName = None
                else:
                    tmp.securityName = securityDto['SecurityName']
                if securityDto['IssueDate'] == '' or securityDto['IssueDate'] is None:
                    tmp.issueDate = None
                else:
                    tmp.issueDate = securityDto['IssueDate']
                if securityDto['TradingUnit'] == '' or securityDto['TradingUnit'] is None:
                    tmp.tradingUnit = None
                else:
                    tmp.tradingUnit = int(securityDto['TradingUnit'])
                if securityDto['ParValue'] == '' or securityDto['ParValue'] is None:
                    tmp.parValue = None
                else:
                    tmp.parValue = int(securityDto['ParValue'])
                if securityDto['UnderlyingSymbol'] == '' or securityDto['UnderlyingSymbol'] is None:
                    tmp.underlyingSymbol = None
                else:
                    tmp.underlyingSymbol = securityDto['UnderlyingSymbol']
                if securityDto['IssuerName'] == '' or securityDto['IssuerName'] is None:
                    tmp.issuerName = None
                else:
                    tmp.issuerName = securityDto['IssuerName']
                if securityDto['CoverredWarrantType'] == '' or securityDto['CoverredWarrantType'] is None:
                    tmp.coveredWarrantType = None
                else:
                    tmp.coveredWarrantType = securityDto['CoverredWarrantType']
                if securityDto['MaturityDate'] == '' or securityDto['MaturityDate'] is None:
                    tmp.maturityDate = None
                else:
                    tmp.maturityDate = securityDto['MaturityDate']
                if securityDto['FirstTradingDate'] == '' or securityDto['FirstTradingDate'] is None:
                    tmp.firstTradingDate = None
                else:
                    tmp.firstTradingDate = securityDto['FirstTradingDate']
                if securityDto['LastTradingDate'] == '' or securityDto['LastTradingDate'] is None:
                    tmp.lastTradingDate = None
                else:
                    tmp.lastTradingDate = securityDto['LastTradingDate']
                if securityDto['ExercisePrice'] == '' or securityDto['ExercisePrice'] is None:
                    tmp.exercisePrice = None
                else:
                    tmp.exercisePrice = float(securityDto['ExercisePrice'])
                if securityDto['ExerciseRatio'] == '' or securityDto['ExerciseRatio'] is None:
                    tmp.exerciseRatio = None
                else:
                    tmp.exerciseRatio = int(securityDto['ExerciseRatio'])
                if securityDto['ListedShare'] == '' or securityDto['ListedShare'] is None:
                    tmp.listedShare = None
                else:
                    tmp.listedShare = float(securityDto['ListedShare'])
                if securityDto['OpenInterest'] == '' or securityDto['OpenInterest'] is None:
                    tmp.openInterest = None
                else:
                    tmp.openInterest = float(securityDto['OpenInterest'])
                if securityDto['OpenInterestChange'] == '' or securityDto['OpenInterestChange'] is None:
                    tmp.openInterestChange = None
                else:
                    tmp.openInterestChange = float(securityDto['OpenInterestChange'])
                if securityDto['SecSessionCode'] == '' or securityDto['SecSessionCode'] is None:
                    tmp.secSessionCode = None
                else:
                    tmp.secSessionCode = securityDto['SecSessionCode']
                if securityDto['TradingStatus'] == '' or securityDto['TradingStatus'] is None:
                    tmp.tradingStatus = None
                else:
                    tmp.tradingStatus = securityDto['TradingStatus']
                if securityDto['TradingStatus'] == '' or securityDto['TradingStatus'] is None:
                    tmp.securityTradingStatus = None
                else:
                    tmp.securityTradingStatus = securityDto['TradingStatus']
                if securityDto['CaStatus'] == '' or securityDto['CaStatus'] is None:
                    tmp.cAStatus = None
                else:
                    tmp.cAStatus = securityDto['CaStatus']
                if securityDto['CaStatus'] == '' or securityDto['CaStatus'] is None:
                    tmp.referenceStatus = None
                else:
                    tmp.referenceStatus = securityDto['CaStatus']
                if securityDto['RefPrice'] == '' or securityDto['RefPrice'] is None:
                    tmp.refPrice = None
                else:
                    tmp.refPrice = float(securityDto['RefPrice'])
                if securityDto['Ceiling'] == '' or securityDto['Ceiling'] is None:
                    tmp.ceilingPrice = None
                else:
                    tmp.ceilingPrice = securityDto['Ceiling']
                if securityDto['Floor'] == '' or securityDto['Floor'] is None:
                    tmp.floorPrice = None
                else:
                    tmp.floorPrice = securityDto['Floor']
                if securityDto['FloorPricePt'] == '' or securityDto['FloorPricePt'] is None:
                    tmp.floorPricePricePt = None
                else:
                    tmp.floorPricePricePt = securityDto['FloorPricePt']
                if securityDto['CeilingPricePt'] == '' or securityDto['CeilingPricePt'] is None:
                    tmp.ceilingPricePt = None
                else:
                    tmp.ceilingPricePricePt = securityDto['CeilingPricePt']
                if securityDto['MatchedPrice'] == '' or securityDto['MatchedPrice'] is None:
                    tmp.matchPrice = None
                else:
                    tmp.matchPrice = securityDto['MatchedPrice']
                if securityDto['MatchedVolume'] == '' or securityDto['MatchedVolume'] is None:
                    tmp.matchQTTY = None
                else:
                    tmp.matchQTTY = securityDto['MatchedVolume']
                if securityDto['PriceChange'] == '' or securityDto['PriceChange'] is None:
                    tmp.priceChange = None
                else:
                    tmp.priceChange = securityDto['PriceChange']
                if securityDto['PriceChangePercentage'] == '' or securityDto['PriceChangePercentage'] is None:
                    tmp.priceChangePercentage = None
                else:
                    tmp.priceChangePercentage = securityDto['PriceChangePercentage']
                if securityDto['PriorClosePrice'] == '' or securityDto['PriorClosePrice'] is None:
                    tmp.priorClosePrice = None
                else:
                    tmp.priorClosePrice = float(securityDto['PriorClosePrice'])
                if securityDto['PriorCloseDate'] == '' or securityDto['PriorCloseDate'] is None:
                    tmp.priorCloseDate = None
                else:
                    tmp.priorCloseDate = securityDto['PriorCloseDate']
                if securityDto['Highest'] == '' or securityDto['Highest'] is None:
                    tmp.highestPrice = None
                else:
                    tmp.highestPrice = float(securityDto['Highest'])
                if securityDto['Lowest'] == '' or securityDto['Lowest'] is None:
                    tmp.lowestPrice = None
                else:
                    tmp.lowestPrice = float(securityDto['Lowest'])
                if securityDto['AvgPrice'] == '' or securityDto['AvgPrice'] is None:
                    tmp.avgPrice = None
                else:
                    tmp.avgPrice = float(securityDto['AvgPrice'])
                if securityDto['OfferCount'] == '' or securityDto['OfferCount'] is None:
                    tmp.offerCount = None
                else:
                    tmp.offerCount = int(securityDto['OfferCount'])
                if securityDto['TotalBidQty'] == '' or securityDto['TotalBidQty'] is None:
                    tmp.totalBidQty = None
                else:
                    tmp.totalBidQty = securityDto['TotalBidQty']
                if securityDto['TotalOfferQty'] == '' or securityDto['TotalOfferQty'] is None:
                    tmp.totalOfferQty = None
                else:
                    tmp.totalOfferQty = securityDto['TotalOfferQty']
                if securityDto['BidCount'] == '' or securityDto['BidCount'] is None:
                    tmp.bidCount = None
                else:
                    tmp.bidCount = securityDto['BidCount']
                if securityDto['NmTotalTradedQty'] == '' or securityDto['NmTotalTradedQty'] is None:
                    tmp.nmTotalTradedQTTY = None
                else:
                    tmp.nmTotalTradedQTTY = float(securityDto['NmTotalTradedQty'])
                if securityDto['NmTotalTradedValue'] == '' or securityDto['NmTotalTradedValue'] is None:
                    tmp.nmTotalTradedValue = None
                else:
                    tmp.nmTotalTradedValue = float(securityDto['NmTotalTradedValue'])
                if securityDto['PtMatchQty'] == '' or securityDto['PtMatchQty'] is None:
                    tmp.ptMatchQTTY = None
                else:
                    tmp.ptMatchQTTY = float(securityDto['PtMatchQty'])
                if securityDto['PtMatchPrice'] == '' or securityDto['PtMatchPrice'] is None:
                    tmp.ptMatchPrice = None
                else:
                    tmp.ptMatchPrice = float(securityDto['PtMatchPrice'])
                if securityDto['PtTotalTradedQty'] == '' or securityDto['PtTotalTradedQty'] is None:
                    tmp.ptTotalTradedQTTY = None
                else:
                    tmp.ptTotalTradedQTTY = float(securityDto['PtTotalTradedQty'])
                if securityDto['PtTotalTradedValue'] == '' or securityDto['PtTotalTradedValue'] is None:
                    tmp.ptTotalTradedValue = None
                else:
                    tmp.ptTotalTradedValue = float(securityDto['PtTotalTradedValue'])
                if securityDto['TotalBuyTradingQtty'] == '' or securityDto['TotalBuyTradingQtty'] is None:
                    tmp.totalBuyTradingQTTY = None
                else:
                    tmp.totalBuyTradingQTTY = float(securityDto['TotalBuyTradingQtty'])
                if securityDto['BuyCount'] == '' or securityDto['BuyCount'] is None:
                    tmp.buyCount = None
                else:
                    tmp.buyCount = int(securityDto['BuyCount'])
                if securityDto['TotalBuyTradingValue'] == '' or securityDto['TotalBuyTradingValue'] is None:
                    tmp.totalBuyTradingValue = None
                else:
                    tmp.totalBuyTradingValue = float(securityDto['TotalBuyTradingValue'])
                if securityDto['TotalSellTradingQtty'] == '' or securityDto['TotalSellTradingQtty'] is None:
                    tmp.totalSellTradingQTTY = None
                else:
                    tmp.totalSellTradingQTTY = float(securityDto['TotalSellTradingQtty'])
                if securityDto['SellCount'] == '' or securityDto['SellCount'] is None:
                    tmp.sellCount = None
                else:
                    tmp.sellCount = int(securityDto['SellCount'])
                if securityDto['TotalSellTradingValue'] == '' or securityDto['TotalSellTradingValue'] is None:
                    tmp.totalSellTradingValue = None
                else:
                    tmp.totalSellTradingValue = float(securityDto['TotalSellTradingValue'])
                if securityDto['BuyForeignQtty'] == '' or securityDto['BuyForeignQtty'] is None:
                    tmp.buyForeignQTTY = None
                else:
                    tmp.buyForeignQTTY = float(securityDto['BuyForeignQtty'])
                if securityDto['BuyForeignValue'] == '' or securityDto['BuyForeignValue'] is None:
                    tmp.buyForeignValue = None
                else:
                    tmp.buyForeignValue = float(securityDto['BuyForeignValue'])
                if securityDto['SellForeignQtty'] == '' or securityDto['SellForeignQtty'] is None:
                    tmp.sellForeignQTTY = None
                else:
                    tmp.sellForeignQTTY = float(securityDto['SellForeignQtty'])
                if securityDto['SellForeignValue'] == '' or securityDto['SellForeignValue'] is None:
                    tmp.sellForeignValue = None
                else:
                    tmp.sellForeignValue = float(securityDto['SellForeignValue'])
                if securityDto['RemainForeignQtty'] == '' or securityDto['RemainForeignQtty'] is None:
                    tmp.remainForeignQTTY = None
                else:
                    tmp.remainForeignQTTY = int(securityDto['RemainForeignQtty'])
                if securityDto['TotalBidQttyOd'] == '' or securityDto['TotalBidQttyOd'] is None:
                    tmp.totalBidQTTYOD = None
                else:
                    tmp.totalBidQTTYOD = securityDto['TotalBidQttyOd']
                if securityDto['TotalOfferQttyOd'] == '' or securityDto['TotalOfferQttyOd'] is None:
                    tmp.totalOfferQTTYOD = None
                else:
                    tmp.totalOfferQTTYOD = int(securityDto['TotalOfferQttyOd'])
                if securityDto['SecSessionStatus'] == '' or securityDto['SecSessionStatus'] is None:
                    tmp.secSessionStatus = None
                else:
                    tmp.secSessionStatus = securityDto['SecSessionStatus']
                if securityDto['PutPrice'] == '' or securityDto['PutPrice'] is None:
                    tmp.putPrice = None
                else:
                    tmp.putPrice = securityDto['PutPrice']
                if securityDto['PutVolumn'] == '' or securityDto['PutVolumn'] is None:
                    tmp.putVolumn = None
                else:
                    tmp.putVolumn = securityDto['PutVolumn']
                if securityDto['MsgType'] == '' or securityDto['MsgType'] is None:
                    tmp.msgType = None
                else:
                    tmp.msgType = securityDto['MsgType']
                if securityDto['SecSession'] == '' or securityDto['SecSession'] is None:
                    tmp.secSession = None
                else:
                    tmp.secSession = securityDto['SecSession']
                # indexInfo
                tmp.indexId = None
                tmp.indexValue = None
                tmp.tradingDate = None
                tmp.calTime = None
                tmp.change = None
                tmp.ratioChange = None
                tmp.totalTrade = None
                tmp.totalQtty = None
                tmp.totalValue = None
                tmp.typeIndex = None
                tmp.indexName = None
                tmp.advances = None
                tmp.nochanges = None
                tmp.declines = None
                tmp.ceilingPrice = None
                tmp.floorPrice = None
                tmp.indexTotalStock = None
                tmp.priorIndexVal = None
                tmp.highestPriceIndex = None
                tmp.lowestPriceIndex = None
                tmp.closeIndex = None
                tmp.msgType = None
                # boardInfo
                # tmp.boardCode = None
                # tmp.marketCode = None
                tmp.session = None
                tmp.tradingSessionID = None
                tmp.tradeStatus = None
                tmp.time = None
                tmp.secSessionCode = None
                # tmp.secSessionStatus = None
                tmp.msgType = None
                tmp.boardStatus = None
                tmp.name = None
                tmp.shortName = None
                tmp.totalTrade = None
                tmp.totalStock = None
                tmp.numSymbolAdvances = None
                tmp.numSymbolNoChange = None
                tmp.numSymbolDeclines = None
                tmp.dateNo = None
                tmp.totalNormalTradedQTTYRD = None
                tmp.totalNormalTradedValueRD = None
                tmp.totalNormalTradedQTTYOd = None
                tmp.totalNormalTradedValueOd = None
                tmp.totalPTTradedQTTY = None
                tmp.totalPTTradedValue = None
                # other
                tmp.weighted = None
                tmp.addDate = None
                # tmp.symbol = None
                # tmp.symbolID = None
                tmp.currentStatus = None
                tmp.indexCode = None
                tmp.listingStatus = None
                tmp.openPrice = None
                tmp.closePrice = None
                tmp.currentPrice = None
                tmp.currentQTTY = None
                tmp.bestOfferPrice = None
                tmp.bestBidPrice = None
                tmp.bestOfferQTTY = None
                tmp.bestBidQTTY = None
                tmp.priorPrice = None
                tmp.totalVolumeTraded = None
                tmp.totalValueTraded = None
                tmp.couponRate = None
                tmp.securityDesc = None
                tmp.tradSesStatus = None
                tmp.stockType = None
                tmp.adjustQTTY = None
                tmp.adjustRate = None
                tmp.dividentRate = None
                tmp.totalOfferQTTY = None
                tmp.totalBidQTTY = None
                # tmp.matchPrice = None
                # tmp.matchQTTY = None
                tmp.deleted = None
                tmp.dateCreated = date.today()
                tmp.dateModified = date.today()
                tmp.modifiedBy = "Admin"
                tmp.createdBy = "Admin"
                tmp.best1Bid = None
                tmp.best2Bid = None
                tmp.best3Bid = None
                tmp.best4Bid = None
                tmp.best5Bid = None
                tmp.best6Bid = None
                tmp.best7Bid = None
                tmp.best8Bid = None
                tmp.best9Bid = None
                tmp.best10Bid = None
                tmp.best1BidVol = None
                tmp.best2BidVol = None
                tmp.best3BidVol = None
                tmp.best4BidVol = None
                tmp.best5BidVol = None
                tmp.best6BidVol = None
                tmp.best7BidVol = None
                tmp.best8BidVol = None
                tmp.best9BidVol = None
                tmp.best10BidVol = None
                tmp.best1Offer = None
                tmp.best2Offer = None
                tmp.best3Offer = None
                tmp.best4Offer = None
                tmp.best5Offer = None
                tmp.best6Offer = None
                tmp.best7Offer = None
                tmp.best8Offer = None
                tmp.best9Offer = None
                tmp.best10Offer = None
                tmp.best1OfferVol = None
                tmp.best2OfferVol = None
                tmp.best3OfferVol = None
                tmp.best4OfferVol = None
                tmp.best5OfferVol = None
                tmp.best6OfferVol = None
                tmp.best7OfferVol = None
                tmp.best8OfferVol = None
                tmp.best9OfferVol = None
                tmp.best10OfferVol = None
            self.stockService.setHNXStocksInfoHNX(tmp)
        print("success")
