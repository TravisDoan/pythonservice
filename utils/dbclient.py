from constants import dbconfig
from model import singleton, singletonthread, hnx

import cx_Oracle


def connect():
    conn = cx_Oracle.connect(dbconfig.DEFAULT_DEV_USER, dbconfig.DEV_PASSWORD, dbconfig.DEV_CONNECT_STRING)
    # curs = conn.cursor()
    return conn


class DBClient(metaclass=singletonthread.Singleton):
    pass

    # connection = cx_Oracle.connect(dbconfig.LOCAL_USER, dbconfig.LOCAL_PASSWORD, dbconfig.CONNECT_STRING)

    def __init__(self):
        self.conn = connect()
        # self.curs = connect()[1]
        self.ping = 'pong'

    @staticmethod
    def test():
        a = "2"
        b = "3"
        return a, b


def main():
    # print(database.LOCAL_CONNECT_STRING)
    print(DBClient.test()[0])
    b = DBClient()

    # cursor = b.connection.cursor()
    # sql = """
    #     SELECT *
    #     FROM tb_HNX_MarketsInfo"""
    # cursor.execute(sql)
    # res = cursor.fetchall()
    # for row in res:
    #     print(row)


if __name__ == '__main__':
    main()
