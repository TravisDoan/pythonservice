from model import singletonthread
from service import stock


class AmqClient(metaclass=singletonthread.Singleton):
    pass

    ss = stock.StockService()

    def __init__(self):
        self.ping = 'pong'

    def saveHNXInfo(self, args):
        self.ss.setHNXStocksInfoHNX(args)

    def saveHNXMarketInfo(self, args):
        self.ss.setHNXMarketsInfo(args)
