from model import stockinfo, stockinfo_pb2

filePath = "E:\Workspace\Python\PythonService\constants\mock.dat"

with open(filePath, "rb") as f:
    stk = stockinfo.List_AmqDto()
    binData = f.read()
    print(binData)
    stk.parse_from_bytes(binData)
    for item in stk.items:
        objType = item.ObjectType
        if objType == "FixBoardInfoDto":
            dtk = stockinfo.FixBoardInfoDto()
            dtk.parse_from_bytes(item.Data)
            print(dtk.Exchange)
            print(dtk.Session)
            print(dtk.TradingSession)
            print(dtk.TradeStatus)
            print(dtk.DateNo)
