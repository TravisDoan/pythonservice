from utils import dbclient
from constants import sql
from model import hose, hnx
from datetime import datetime, date

import enum
import time


class Market(enum.Enum):
    HNX = 1
    HOSE = 2
    UP_COM = 3


class StockService(object):
    def __init__(self):
        self.ping = 'pong'
        self.conn = dbclient.DBClient().conn
        self.ping = 'pong'

    @staticmethod
    def getAllHNXStocksInfo(self):
        curs = self.conn.cursor()
        sqlStr = sql.QUERY_SELECT_ALL_HNX_STOCK_INFO
        curs.execute(sqlStr)
        res = curs.fetchall()
        return res

    @staticmethod
    def getAllHNXMarketsInfo(self):
        curs = self.conn.cursor()
        sqlStr = sql.QUERY_SELECT_ALL_HNX_MARKET_INFO
        curs.execute(sqlStr)
        res = curs.fetchall()
        return res

    @staticmethod
    def getAllUpComStocksInfo(self):
        curs = self.conn.cursor()
        sqlStr = sql.QUERY_SELECT_ALL_UPCOM_STOCK_INFO
        curs.execute(sqlStr)
        res = curs.fetchall()
        return res

    @staticmethod
    def getAllUpComMarketsInfo(self):
        curs = self.conn.cursor()
        sqlStr = sql.QUERY_SELECT_ALL_UPCOM_MARKET_INFO
        curs.execute(sqlStr)
        res = curs.fetchall()
        return res

    @staticmethod
    def getAllHoseTrading(self):
        curs = self.conn.cursor()
        sqlStr = sql.QUERY_SELECT_ALL_HOSE_TRADING
        curs.execute(sqlStr)
        res = curs.fetchall()
        return res

    @staticmethod
    def getAllHoseDailyMarketData(self):
        curs = self.conn.cursor()
        sqlStr = sql.QUERY_SELECT_ALL_HOSE_DAILY_MARKET_DATA
        curs.execute(sqlStr)
        res = curs.fetchall()
        return res

    @staticmethod
    def getAllHoseTotalTrading(self):
        curs = self.conn.cursor()
        sqlStr = sql.QUERY_SELECT_ALL_HOSE_TOTAL_TRADING
        curs.execute(sqlStr)
        res = curs.fetchall()
        return res

    @staticmethod
    def getAllHoseForeignerTrading(self):
        curs = self.conn.cursor()
        sqlStr = sql.QUERY_SELECT_ALL_HOSE_FOREIGNER_TRADING
        curs.execute(sqlStr)
        res = curs.fetchall()
        return res

    def setHNXStocksInfoHNX(self, obj):
        curs = self.conn.cursor()
        curs.callproc(sql.PROCEDURE_INSERT_HNX_STOCK_INFO, (
            obj.tradingDate, obj.symbolID, obj.symbol, obj.boardCode, obj.boardStatus, obj.securityType,
            obj.tradingSessionID,
            obj.tradSesStatus, obj.stockType, obj.tradingUnit, obj.listingStatus, obj.adjustQTTY, obj.referenceStatus,
            obj.adjustRate, obj.dividentRate, obj.securityTradingStatus, obj.openPrice, obj.closePrice,
            obj.currentPrice,
            obj.currentQTTY, obj.highestPrice, obj.lowestPrice, obj.bestOfferPrice, obj.bestBidPrice, obj.ceilingPrice,
            obj.floorPrice,
            obj.totalOfferQTTY, obj.totalBidQTTY, obj.totalBidQTTYOD, obj.totalOfferQTTYOD, obj.bestOfferQTTY,
            obj.bestBidQTTY, obj.priorPrice,
            obj.priorClosePrice, obj.matchPrice, obj.matchQTTY, obj.deleted, obj.dateCreated, obj.dateModified,
            obj.modifiedBy, obj.createdBy,
            obj.name, obj.shortName, obj.parValue, obj.marketCode, obj.dateNo, obj.offerCount, obj.bidCount,
            obj.nmTotalTradedQTTY,
            obj.nmTotalTradedValue, obj.ptMatchQTTY, obj.ptMatchPrice, obj.ptTotalTradedQTTY, obj.ptTotalTradedValue,
            obj.totalBuyTradingQTTY, obj.buyCount, obj.totalBuyTradingValue, obj.totalSellTradingQTTY, obj.sellCount,
            obj.totalSellTradingValue, obj.totalVolumeTraded, obj.totalValueTraded, obj.tradingTime, obj.issuerName,
            obj.issueDate,
            obj.maturityDate, obj.couponRate, obj.securityDesc, obj.buyForeignQTTY, obj.buyForeignValue,
            obj.sellForeignQTTY,
            obj.sellForeignValue, obj.remainForeignQTTY, obj.totalStock, obj.totalTrade, obj.numSymbolAdvances,
            obj.numSymbolNoChange,
            obj.numSymbolDeclines, obj.totalNormalTradedQTTYRD, obj.totalNormalTradedValueRD,
            obj.totalNormalTradedQTTYOd,
            obj.totalNormalTradedValueOd, obj.totalPTTradedQTTY, obj.totalPTTradedValue, obj.underlyingSymbol,
            obj.firstTradingDate,
            obj.lastTradingDate, obj.coveredWarrantType, obj.exercisePrice, obj.exerciseRatio, obj.listedShare,
            obj.openInterest,
            obj.openInterestChange, obj.secSession, obj.cAStatus, obj.refPrice, obj.avgPrice, obj.best1Bid,
            obj.best2Bid, obj.best3Bid, obj.best4Bid,
            obj.best5Bid, obj.best6Bid, obj.best7Bid, obj.best8Bid, obj.best9Bid, obj.best10Bid, obj.best1BidVol,
            obj.best2BidVol, obj.best3BidVol,
            obj.best4BidVol, obj.best5BidVol, obj.best6BidVol, obj.best7BidVol, obj.best8BidVol, obj.best9BidVol,
            obj.best10BidVol, obj.best1Offer,
            obj.best2Offer, obj.best3Offer, obj.best4Offer, obj.best5Offer, obj.best6Offer, obj.best7Offer,
            obj.best8Offer, obj.best9Offer,
            obj.best10Offer, obj.best1OfferVol, obj.best2OfferVol, obj.best3OfferVol, obj.best4OfferVol,
            obj.best5OfferVol, obj.best6OfferVol,
            obj.best7OfferVol, obj.best8OfferVol, obj.best9OfferVol, obj.best10OfferVol, obj.priceChange,
            obj.priceChangePercentage))

    def setHNXMarketsInfo(self, obj):
        curs = self.conn.cursor()
        print(obj.calTime)
        curs.callproc(sql.PROCEDURE_INSERT_HNX_MARKET_INFO, (
            obj.indexID, obj.indexCode, obj.indexValue, obj.calTime, obj.change, obj.ratioChange, obj.totalQTTY,
            obj.totalValue, obj.tradingDate, obj.currentStatus, obj.totalStock, obj.priorIndexVal, obj.highestIndex,
            obj.lowestIndex, obj.closeIndex, obj.typeIndex, obj.indexName, obj.weighted, obj.addDate, obj.symbolID,
            obj.symbol, obj.advances, obj.noChanges, obj.declines, obj.ceiling, obj.floor, obj.totalNormalTradedQTTYOD,
            obj.totalNormalTradedValueOd, obj.totalPTTradedQTTY, obj.totalPTTradedValue))

    @staticmethod
    def setUpComStocksInfo(self, obj):
        curs = self.conn.cursor()
        curs.callproc(sql.PROCEDURE_INSERT_UPCOM_STOCK_INFO, (
            obj.stockInfoID, obj.tradingDate, obj.time, obj.stockID, obj.code, obj.stockType, obj.totalListingQtty,
            obj.tradingUnit, obj.listtingStatus, obj.adjustQtty, obj.referenceStatus, obj.adjustRate, obj.dividentRate,
            obj.status, obj.totalRoom, obj.currentRoom, obj.basicPrice, obj.openPrice, obj.closePrice, obj.currentPrice,
            obj.currentQtty, obj.highestPrice, obj.lowestPrice, obj.bestOfferPrice, obj.bestBidPrice, obj.ceilingPrice,
            obj.floorPrice, obj.totalOfferQtty, obj.totalBidQtty, obj.bestOfferQtty, obj.bestBidQtty, obj.priorPrice,
            obj.priorClosePrice, obj.matchPrice, obj.matchQtty, obj.deleted, obj.dateCreated, obj.dateModified,
            obj.modifiedBy, obj.createdBy, obj.name, obj.parValue, obj.floorCode, obj.matchValue, obj.isCalcIndex,
            obj.isDeterminecl, obj.dateNo, obj.offerCount, obj.bidCount, obj.averagePrice, obj.indexPrice,
            obj.prevPriorPrice, obj.yieldmat, obj.prevPriorClosePrice, obj.nmTotalTradedQtty, obj.nmTotalTradedValue,
            obj.ptMatchQtty, obj.ptMatchPrice, obj.ptTotalTradedQtty, obj.ptTotalTradedValue, obj.totalBuyTradingQtty,
            obj.buyCount, obj.totalBuyTradingValue, obj.totalSellTradingQtty, obj.sellCount, obj.totalSellTradingValue,
            obj.totalTradingQtty, obj.totalTradingValue, obj.buyForeignQtty, obj.buyForeignValue, obj.sellForeignQtty,
            obj.sellForeignValue, obj.remainForeignQtty, obj.ptYieldmat, obj.dateReport, obj.closePriceAdjusted,
            obj.rateAdjusted, obj.highestAdjusted, obj.lowestAdjusted, obj.openPriceAdjusted, obj.ceilingRate,
            obj.floorRate, obj.isPrivateRateUsed, obj.totalCustQtty, obj.auMatchQtty, obj.auMatchPrice,
            obj.auTotalTradedQtty, obj.auTotalTradedValue))

    @staticmethod
    def setUpComMarketsInfo(self, obj):
        curs = self.conn.cursor()
        curs.callproc(sql.PROCEDURE_INSERT_UPCOM_MARKET_INFO, (
            obj.marketInfoID, obj.floorCode, obj.dateNo, obj.tradingDate, obj.currentStatus, obj.time, obj.totalTrade,
            obj.totalStock, obj.advances, obj.declines, obj.nochange, obj.totalQtty, obj.totalValue,
            obj.priorMarketIndex, obj.chgIndex, obj.pctIndex, obj.currentIndex, obj.marketIndex, obj.sessionNo,
            obj.lowest, obj.highest, obj.prvPriorMarketIndex, obj.avrMarketIndex, obj.avrPriorMarketIndex,
            obj.avrChgIndex, obj.avrPctIndex, obj.ptTotalQtty, obj.ptTotalValue, obj.ptTotalTrade, obj.dateReport,
            obj.openIndex))

    @staticmethod
    def setHoseTrading(self, obj):
        curs = self.conn.cursor()
        curs.callproc(sql.PROCEDURE_INSERT_HOSE_TRADING, (
            obj.stockNo, obj.stockSymbol, obj.stockType, obj.ceiling, obj.floor, obj.bigLotValue, obj.securityName,
            obj.sectorNo, obj.designated, obj.suspension, obj.delist, obj.haltResumeFlag, obj.split, obj.benefit,
            obj.meeting, obj.notice, obj.clientIDRequest, obj.couponRate, obj.issueDate, obj.matureDate, obj.avrPrice,
            obj.parValue, obj.sdcflag, obj.priorClosePrice, obj.priorCloseDate, obj.projectOpen, obj.openPrice,
            obj.last, obj.lastVol, obj.lastVal, obj.highest, obj.lowest, obj.totalShare, obj.totalValue,
            obj.accumulateDeal, obj.bigDeal, obj.bigVol, obj.bigVal, obj.oddDeal, obj.oddVol, obj.oddVal, obj.best1Bid,
            obj.best1BidVolume, obj.best2Bid, obj.best2BidVolume, obj.best3Bid, obj.best3BidVolume, obj.best1Offer,
            obj.best1OfferVolume, obj.best2Offer, obj.best2OfferVolume, obj.best3Offer, obj.best3OfferVolume,
            obj.dateReport, obj.currentRoom, obj.totalRoom, obj.match1, obj.matchVolume1, obj.match2, obj.matchVolume2,
            obj.other_room, obj.room, obj.sellPrice3, obj.sellPrice2, obj.sellPrice1, obj.buyPrice3, obj.buyPrice2,
            obj.buyPrice1, obj.sellQtty3, obj.sellQtty2, obj.sellQtty1, obj.buyQtty3, obj.buyQtty2, obj.buyQtty1,
            obj.changePrice, obj.closePrice_Adjusted, obj.rate_Adjusted, obj.highest_Adjusted, obj.lowest_Adjusted,
            obj.klgd_ThoaThuan, obj.gtgd_ThoaThuan, obj.openPrice_Adjusted))

    @staticmethod
    def setHoseDailyMarketData(self, obj):
        curs = self.conn.cursor()
        curs.callproc(sql.PROCEDURE_INSERT_HOSE_DAILY_MARKET_DATA, (
            obj.vnIndex, obj.totalTrade, obj.totalShares, obj.totalValues, obj.upVolume, obj.downVolume,
            obj.noChangeVolume, obj.advances, obj.declines, obj.noChange, obj.vn50Index, obj.marketID, obj.filter,
            obj.time, obj.dateReport, obj.preVNIndex, obj.indexChange, obj.liveID, obj.lowest, obj.highest,
            obj.openIndex))

    @staticmethod
    def setHoseTotalTrading(self, obj):
        curs = self.conn.cursor()
        curs.callproc(sql.PROCEDURE_INSERT_HOSE_TOTAL_TRADING, (
            obj.ticker, obj.exchangeID, obj.openPrice, obj.highest, obj.lowest, obj.avgPrice, obj.closePrice,
            obj.tradingDate, obj.changePrice, obj.matchBuyTradeValue, obj.matchBuyTradeQuantity,
            obj.matchSellTradeValue, obj.matchSellTradeQuantity, obj.buySell, obj.totalTrade, obj.totalValue))

    @staticmethod
    def setHoseForeignerTrading(self, obj):
        curs = self.conn.cursor()
        curs.callproc(sql.PROCEDURE_INSERT_HOSE_FOREIGNER_TRADING, (
            obj.foreignTradingResultID, obj.stockCode, obj.remainForeignQtty, obj.buyForeignQtty, obj.buyForeignValue,
            obj.sellForeignQtty, obj.sellForeignValue, obj.tradingDate, obj.buyForeignQttyKL, obj.buyForeignValueKL,
            obj.sellForeignQttyKL, obj.sellForeignValueKL, obj.buyForeignQttyTT, obj.buyForeignValueTT,
            obj.sellForeignQttyTT, obj.sellForeignValueTT))


def main():
    stockInfo = hnx.MarketsInfoHNX(1, 'ssi', 1, 'ssi', 1, 1, 1, 1, date.today(), 'ssi', 1, 1, 1, 1, 1, 'test', 'ssi',
                                   1, date.today(), 1, 'ssi', 1, 1, 1, 1, 1, 1, 1,
                                   None, 1)
    # stockInfo = hnx.MarketsInfoHNX(1, 'ssi', 1, 'ssi', 1, 1, 1, 1, date.today(), 'ssi', 1, 1, 1, 1, 1, 'test', 'ssi',
    #                                1, date.today(), 1, 'ssi', 1, 1, 1, 1, 1, 1, 1,
    #                                None, 1)
    stockService = StockService()
    stockService.setHNXMarketsInfo(stockInfo)


if __name__ == '__main__':
    main()
