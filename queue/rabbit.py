from utils import protoclient, projsonclient
from stomp import ConnectionListener
from model import stockinfo

import os
import json
import time
import stomp


def connect_and_subscribe(conn):
    conn.start()
    conn.connect(None, None, wait=True)
    conn.subscribe(destination='/queue/celery', id=1, ack='auto')
    # conn.send('/queue/daily.data', 'hello')

class MyListener(ConnectionListener):
    def __init__(self, conn):
        self.conn = conn

    def on_error(self, headers, message):
        print('received an error "%s"' % message)

    def on_message(self, headers, message):
        # print
        # print(base64.b64decode(message.encode()))
        # print(json.loads(message))
        print(message)

        # print(message.encode())
        # ecode = base64.b64decode(message.encode())
        # f = open("E:\Workspace\Python\PythonService\constants\mock.dat", "wb")
        # f.write(base64.b64decode(message.encode()))
        # f.close()
        # with open("E:\Workspace\Python\PythonService\constants\mock.dat", "rb") as strem:
        #     stk = stockinfo.List_AmqDto()
        #     binData = strem.read()
        #     print(binData)
        #     stk.parse_from_bytes(binData)
        #     print(stk)
        #     for item in stk.items:
        #         objType = item.ObjectType
        #         if objType == "FixBoardInfoDto":
        #             dtk = stockinfo.FixBoardInfoDto()
        #             dtk.parse_from_bytes(item.Data)
        #             print(dtk.Exchange)
        #             print(dtk.Session)
        #             print(dtk.TradingSession)
        #             print(dtk.TradeStatus)
        #             print(dtk.DateNo)

        # protoClient = protoclient.ProtoClient()
        # protoClient.initSerialize(message.encode())

        # projsonClient = projsonclient.ProjsonClient()
        # projsonClient.initSerialize(message)
        print('processed message')



def on_disconnected(self):
    print('disconnected')
    connect_and_subscribe(self.conn)

conn = stomp.Connection([('localhost', 61616)], auto_content_length=True)
listener = MyListener(conn)
conn.set_listener('', listener)
connect_and_subscribe(conn)
time.sleep(20000)
conn.disconnect()
